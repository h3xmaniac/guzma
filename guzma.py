from config import token
import discord
from discord.ext import commands
import traceback
import os
import aiofiles
from ast import literal_eval
from cogs.lib import uni
try:
    with open('prefixes.txt') as file:
        extradata = literal_eval(file.read())
except:
    with open('prefixes.txt','w') as file:
        extradata = {}
        file.write('{}')
def get_prefix(bot, ctx):
    if type(ctx) == commands.Context:
        ctx = ctx.message
    try:
        extra = bot.prefixes[ctx.guild.id]
    except KeyError:
        extra = None
    except AttributeError:
        extra = ''
    if extra is not None:
        prefixes = [extra, '?','<@493954966638297089> ','<@!493954966638297089> ']
    else:
        prefixes = ['?','<@493954966638297089> ','<@!493954966638297089> ']
    # If we are in a guild, we allow for the user to mention us or use any of the prefixes in our list.
    return prefixes
class ImFinallySubclassingBot(commands.Bot):
    channel = 0
    prefixes = extradata
    async def on_ready(self):
        print('='*40)
        print(f'{"I am":^40}')
        print(f'{f"{self.user} ({self.user.id})":^40}')
        print('='*40)
        self.channel = self.get_channel(493726269079552010)
        for extension in [f.replace('.py', '') for f in os.listdir('cogs') if os.path.isfile(os.path.join('cogs', f))]:
            try:
                self.load_extension("cogs." + extension)
            except (discord.ClientException, ModuleNotFoundError):
                print(f'Failed to load extension {extension}.')
                traceback.print_exc()
        try:
            async with aiofiles.open('restart.txt') as file:
                data = await file.read()
                data = data.split('\n')
                channel = self.get_channel(int(data[0]))
                user = self.get_user(int(data[1]))
                await channel.send(f'{user.mention} I\'m back baby')
                os.remove("restart.txt")
        except:
            pass
    async def on_command_error(self,ctx,error):
        if isinstance(error, commands.errors.CommandNotFound):
            pass
        elif isinstance(error, discord.errors.Forbidden):
            pass
        elif isinstance(error, commands.errors.BotMissingPermissions):
            await ctx.send((await uni.translate(ctx, 'error.bot_permission')).format(await uni.translate(ctx,'misc.permission_roles'))) # generalise this
        elif isinstance(error, commands.errors.CheckFailure):
            await ctx.send(await uni.translate(ctx, 'error.no_permission'))
        elif isinstance(error, commands.errors.MissingRequiredArgument):
            try:
                help = await self.formatter.format_help_for(ctx, ctx.command,sendhelp=False)
                await ctx.send((await uni.translate(ctx, 'error.missing_required'))+"\n" + help[0])
            except Exception as error:
                trace = traceback.format_exception(type(error), error, error.__traceback__)
                out = '```diff\nThis was sent from within MissingRequiredArgument handler\n'
                for i in trace:
                    j = i.split('\n')
                    for k in j:
                        l = '-'+k+'\n'
                        if len(out+l+'```') > 2000:
                            await self.channel.send(out+'```')
                            out = '```diff\n'
                        out += l
                await self.channel.send(out+'```')
        elif isinstance(error, commands.errors.BadArgument):
            await ctx.send(await uni.translate(ctx, 'error.bad'))
        else:
            try:
                if isinstance(error.__cause__, discord.errors.NotFound):
                    return
            except:
                    pass
            await ctx.send((await uni.translate(ctx, 'error.reported')).format(ctx.command.name))
            print("Ignoring exception in command {}".format(ctx.command.name))
            trace = traceback.format_exception(type(error), error, error.__traceback__)
            out = '```diff\n'
            for i in trace:
                j = i.split('\n')
                for k in j:
                    l = '-'+k+'\n'
                    if len(out+l+'```') > 2000:
                        await self.channel.send(out+'```')
                        out = '```diff\n'
                    out += l
            await self.channel.send(out+'```')
            await self.channel.send(f"{ctx.author.id}: {ctx.message.content}")
    async def on_message(self, msg):
        try:
            if msg.author.bot:
                return
            ctx = await self.get_context(msg)
            perms = ctx.channel.permissions_for(ctx.me)
            if perms.value & 114688 != 114688 and perms.value & 8 != 8 and ctx.command is not None:
                try:
                    missing_req = ''
                    if not perms.embed_links:
                        missing_req += await uni.translate(ctx,'misc.permission_embed')
                    if not perms.read_message_history:
                        missing_req += '\n' + await uni.translate(ctx,'misc.permission_history')
                    if not perms.attach_files:
                        missing_req += '\n' + await uni.translate(ctx,'misc.permission_files')
                    if missing_req == '':
                        missing_req = await uni.translate(ctx,'misc.no_missing')
                    else:
                        missing_req = missing_req.strip()
                    missing_add = ''
                    if not perms.manage_messages:
                        missing_add += '\n' + await uni.translate(ctx,'misc.permission_messages')
                    if not perms.add_reactions:
                        missing_add += '\n' + await uni.translate(ctx,'misc.permission_reactions')
                    if not perms.external_emojis:
                        missing_add += '\n' + await uni.translate(ctx,'misc.permission_emoji')
                    if missing_add == '':
                        missing_add = await uni.translate(ctx,'misc.no_missing')
                    else:
                        missing_add = missing_add.strip()
                    await msg.channel.send((await uni.translate(ctx,'misc.missing_permissions')).format(missing_req, missing_add))
                except discord.errors.Forbidden:
                    pass
                return
            if msg.content == '!'*3:
                await msg.channel.send(await uni.translate(ctx,'misc.surprised'))
            elif msg.content == '?'*3:
                await msg.channel.send(await uni.translate(ctx,'misc.confused'))
            await self.process_commands(msg)

        except Exception as error:
            if not isinstance(error, discord.errors.Forbidden):
                try:
                    trace = traceback.format_exception(type(error), error, error.__traceback__)
                    out = '```'
                    for i in trace:
                        if len(out+i+'```') > 2000:
                            await self.channel.send(out+'```')
                            out = '```'
                        out += i
                    await self.channel.send(out+'```')
                except:
                    pass
            await self.process_commands(msg)
bot = ImFinallySubclassingBot(get_prefix)
bot.run(token)